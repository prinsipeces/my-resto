-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 01, 2021 at 11:10 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `restaurant_mgmt`
--

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `name`, `username`, `password`) VALUES
(1, 'cesar francisco', 'admin', 'password');

-- --------------------------------------------------------

--
-- Table structure for table `menuitem`
--

CREATE TABLE `menuitem` (
  `id` int(11) NOT NULL,
  `img` varchar(50) NOT NULL DEFAULT '0',
  `name` varchar(50) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `category` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menuitem`
--

INSERT INTO `menuitem` (`id`, `img`, `name`, `price`, `category`) VALUES
(4, './images/breakfast/beeftapa.png', 'Beef Tapa', '120.00', 'Breakfast'),
(5, './images/ricemeals/burgerst.png', 'Burger Steak with Spaghetti', '150.00', 'Rice Meals'),
(14, './images/noodles/spagfam.png', 'Jolly Spaghetti Family Pan', '85.00', 'Noodles'),
(2, './images/breakfast/baconandegg.png', 'Bacon&Egg Pancake Sandwich', '75.00', 'Breakfast'),
(3, './images/breakfast/cornedbeef.png', 'Corned Beef', '55.00', 'Breakfast'),
(6, './images/ricemeals/brugersteaksp.png', 'Burger Steak Supreme', '125.00', 'Rice Meals'),
(7, './images/ricemeals/burgersteaksh.png', 'Burger Steak with Shanghai', '105.00', 'Rice Meals'),
(8, './images/ricemeals/burgersteaksfd.png', 'Burger Steak with Fries and drink', '79.00', 'Rice Meals'),
(9, './images/chicken/1pcchickenspag.png', '1-pc. Chickenjoy with Jolly Spaghetti', '90.00', 'Chicken Meals'),
(10, './images/chicken/1pcchickenmeal.png', '1-pc. Chickenjoy Value Meal with Fries', '100.00', 'Chicken Meals'),
(11, './images/chicken/1pcchickenbrg.png', '1-pc. Chickenjoy with Burger Steak', '120.00', 'Chicken Meals'),
(12, './images/chicken/1pcchickenval.png', '1-pc. Chickenjoy with Burger Steak and Half Spaghe', '105.00', 'Chicken Meals'),
(13, './images/noodles/jollyspagbrg.png', 'Jolly Spaghetti with Yumburger', '69.00', 'Noodles'),
(15, './images/noodles/jollyspag.png', 'Jollibee Spaghetti', '65.00', 'Noodles'),
(16, './images/noodles/palabok.png', 'Palabok Solo', '50.00', 'Noodles'),
(17, './images/burger/brgaloha.png', 'Amazing Aloha Yumburger', '60.00', 'Burger'),
(18, './images/burger/tlcbrg.png', 'TLC Yumburger', '90.00', 'Burger'),
(19, './images/burger/cheesybaconbrg.png', 'Cheesy Bacon Yumburger', '75.00', 'Burger'),
(20, './images/burger/cheesybrg.png', 'Cheesy Burger', '35.00', 'Burger'),
(1, './images/breakfast/yumburger-breakfast.png', 'Breakfast Yumburger', '45.00', 'Breakfast');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menuitem`
--
ALTER TABLE `menuitem`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menuitem`
--
ALTER TABLE `menuitem`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
