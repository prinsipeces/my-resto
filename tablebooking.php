<?php
  session_start();
  require('./views/layouts/master.view.php');
?>


 <!-- Breadcrumbs -->
  <div class="col s12 container center z-depth-5">
    <a href="#!" class="breadcrumb black-text"><h2>Table For Reservation</h2></a>
    
         <div class = "col s12 z-depth-5">
            <ul class = "tabs">
               <li class = "tab col s3"><a class = "active" href = "#breakfast">Table for 2</a></li>
               <li class = "tab col s3"><a href = "#ricemeals">Table for 4</a></li>
               <li class = "tab col s3"><a href = "#chicken">Table for 6</a></li>
               <li class = "tab col s3"><a href = "#noodles">Family Table</a></li>
            </ul>
         </div>
      
      <!-- Breakfast -->   
      <div id="breakfast">
      <div class="row">
        <div class="card">
          <div class="col s12 m3">
            <div class="card-image">
              <img src="./images/tables/for 2/ftwo-1.jpg" alt="">
              <div><h6>Normal</h6></div><br>
                <span class="red-text"> ₱60.00</span>
                <a href="checkout.php" class="waves-effect waves-light btn-small">Reserve Now</a>
            </div>
          </div>
        </div>

        <div class="card">
          <div class="col s12 m3">
            <div class="card-image">
              <img src="./images/tables/for 2/ftwo-2.jpg">
              <div><h6>Special</h6></div>
            <span class="red-text"> ₱150.00</span>
            <a href="checkout.php" class="waves-effect waves-light btn-small disabled">Reserve Now</a>
            </div>
          </div>  
        </div>

        <div class="card">
          <div class="col s12 m3">
            <div class="card-image">
              <img src="./images/tables/for 2/ftwo-3.jpg">
              <div><h6>Very Special</h6></div><br>
            <span class="red-text"> ₱300.00</span>
            <a href="checkout.php" class="waves-effect waves-light btn-small">Reserve Now</a>
            </div>
          </div>  
        </div>

      </div>  
    </div>


      <!--JavaScript at end of body for optimized loading-->
    <script type="text/javascript" src="js/materialize.min.js"></script>
    
    <script type="text/javascript">
      const sideNav = document.querySelector('.sidenav');
      M.Sidenav.init(sideNav, {});
    </script>

    
    <script type="text/javascript">
      const ss = document.querySelectorAll('.scrollspy');
      M.ScrollSpy.init(ss, {});
    </script>

    </body>
  </html>