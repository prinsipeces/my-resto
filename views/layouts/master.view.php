<!DOCTYPE html>
  <html>
    <head>
      <title>Food Ordering And Table Reservation</title>
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
       <!-- Compiled and minified CSS -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
      <!-- Compiled and minified JavaScript -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>
    <body>
<!-- Nav -->
  <div class="navbar-fixed">
    <nav class="#ff6d00 orange accent-4">
      <div>
        <div class="nav-wrapper">
          <a href="#!" class="brand-logo">My Resto</a>
          <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
           <ul class="right hide-on-med-and-down">
      <li>
              <a href="index.php">Home</a>
            </li>
            <li>
              <a href="menu.php">Menu</a>
            </li>
            <li>
              <a href="tablebooking.php">Table Booking</a>
            </li>
            <li>
              <a href="index.php#gallery">Gallery</a>
            </li>
            <li>
              <a href="cart.php">Cart</a>
            </li>
            <li>
        <a href="index.php#contactus">Contact Us</a>
      </li>
            <li>
              <a href="checkout.php" class="waves-effect waves-light btn-small">Check Out</a>
            </li>

            <?php if(!isset($_SESSION['user'])):?>
            <li>
              <a href="login.php" class="waves-effect waves-light btn-small">Log in</a>
            </li>
            <?php else: ?>
              <li>
                <a href="logout.php" class="waves-effect waves-light btn-small">Log out</a>
              </li>

          <?php endif; ?>
          </ul>
        </div>
      </div>
    </nav>
  </div>
  <ul class="sidenav" id="mobile-demo">
    <li>
      <a href="index.php">Home</a>
    </li>
    <li>
      <a href="menu.php">Menu</a>
    </li>
    <li>
      <a href="tablebooking.php">Table Booking</a>
    </li>
 
    <li>
      <a href="index.php#gallery">Gallery</a>
    </li>
    <li>
      <a href="cart.php">Cart</a>
    </li>
    <li>
      <a href="index.php#contactus">Contact Us</a>
    </li>
     <li>
              <a href="checkout.php" class="waves-effect waves-light btn-small">Check Out</a>
            </li>

            <?php if(!isset($_SESSION['user'])):?>
            <li>
              <a href="Login.page.php" class="waves-effect waves-light btn-small">Log in</a>
            </li>
            <?php else: ?>
              <li>
                <a href="Logout.page.php" class="waves-effect waves-light btn-small">Log out</a>
              </li>
<?php endif; ?>
  </ul>