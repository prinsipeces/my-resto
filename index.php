<?php session_start(); ?>
<?php
  require('./views/layouts/master.view.php');
?>

<!-- Carousel -->
<section id="home" class="slider scrollspy">
    <ul class="slides">
      <li>
        <img src="./images/goodtaste-foods.jpg" alt="">
        <div class="caption center-align">
          <h2>Eat All You Can</h2>
          <h5 class="light grey-text text-lighten-3 hide-on-small-only">Lorem ipsum dolor sit amet consectetur adipisicing elit. Veniam non illo earum cumque id est!</h5>
          <a href="menu.php" class="btn btn-large #ff6d00 orange accent-4">Order Now!</a>
        </div>
      </li>
      <li>
        <img src="./images/2.jpg" alt="">
        <div class="caption left-align">
          <h2>We Work With All Budgets</h2>
          <h5 class="light grey-text text-lighten-3 hide-on-small-only">Lorem ipsum dolor sit amet consectetur adipisicing elit. Veniam non illo earum cumque id est!</h5>
          <a href="menu.php" class="btn btn-large #ff6d00 orange accent-4">Order Now!</a>
        </div>
      </li>
      <li>
        <img src="./images/3.jpg" alt="">
        <div class="caption right-align">
          <h2>Group & Individual</h2>
          <h5 class="light grey-text text-lighten-3 hide-on-small-only">Lorem ipsum dolor sit amet consectetur adipisicing elit. Veniam non illo earum cumque id est!</h5>
          <a href="menu.php" class="btn btn-large #ff6d00 orange accent-4">Order Now!</a>
        </div>
      </li>
    </ul>
  </section>



<!-- Photo Gallery -->
  <section id="gallery" class="section section-gallery scrollspy">
    <div class="container">
      <h4 class="center">
        Photo Gallery
      </h4>
      <div class="row">
        <div class="col s12 m3">
          <img class="materialboxed responsive-img" src="./images/1a.jpeg" alt="">
        </div>
        <div class="col s12 m3">
          <img class="materialboxed responsive-img" src="./images/2a.jpg" alt="">
        </div>
        <div class="col s12 m3">
          <img class="materialboxed responsive-img" src="./images/3a.jpg" alt="">
        </div>
        <div class="col s12 m3">
          <img class="materialboxed responsive-img" src="./images/4a.jpg" alt="">
        </div>
      </div>

      <div class="row">
        <div class="col s12 m3">
          <img class="materialboxed responsive-img" src="./images/5a.jpg" alt="">
        </div>
        <div class="col s12 m3">
          <img class="materialboxed responsive-img" src="./images/6a.jpg" alt="">
        </div>
        <div class="col s12 m3">
          <img class="materialboxed responsive-img" src="./images/7a.jpg" alt="">
        </div>
        <div class="col s12 m3">
          <img class="materialboxed responsive-img" src="./images/8a.jpg" alt="">
        </div>
      </div>

      <div class="row">
        <div class="col s12 m3">
          <img class="materialboxed responsive-img" src="./images/9a.jpg" alt="">
        </div>
        <div class="col s12 m3">
          <img class="materialboxed responsive-img" src="./images/10a.jpg" alt="">
        </div>
        <div class="col s12 m3">
          <img class="materialboxed responsive-img" src="./images/11a.jpg" alt="">
        </div>
        <div class="col s12 m3">
          <img class="materialboxed responsive-img" src="./images/12a.jpeg" alt="">
        </div>
      </div>
    </div>
  </section>

<!-- Footer -->
  <section id="resto" class="section section-icons grey lighten-4 center scrollspy">
    <div class="container">
      <div class="row">
        
        <div class="col s12 m4">
          <div class="card-panel">
            <i class="material-icons large teal-text">store</i>
            <h4>About Us</h4>
            <p>
              Operation Hours: 24/7<br>
              Telephone: +632 443-7959<br>
              Facebook: Good Taste Restaurant & Cafe</p>
          </div>
        </div>

        <div class="col s12 m4">
          <div class="card-panel">
            <i class="material-icons large teal-text">room</i>
            <h4>Located at?</h4>
            <p>GOOD TASTE RESTAURANT & CAFÉ<br>
              Carino St, Baguio, Luzon, Philippines<br><br><br></p>
          </div>
        </div>
        
        <div class="col s12 m4">
          <div class="card-panel">
            <i class="material-icons large teal-text">person_add</i>
            <h4>Community</h4>
            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quidem, velit.<br><br></p>
          </div>
        </div>
      </div>
    </div>
  </section>

<!-- Fill up form -->
  <section id="contactus" class="scrollspy">
    <div class="container">
    <div class="col s12 m6">
            <div class="card-panel grey lighten-3">
              <h5>To contact us, please fill out this form.</h5><br>
              <div class="input-field">
                <input type="text" placeholder="Name" id="name">
                <label for="name">Name</label>
              </div>
              <div class="input-field">
                <input type="email" placeholder="Email" id="email">
                <label for="email">Email</label>
              </div>
              <div class="input-field">
                <input type="text" placeholder="Phone" id="phone">
                <label for="phone">Phone</label>
              </div>
              <div class="input-field">
                <textarea class="materialize-textarea" placeholder="Enter Message" id="message"></textarea>
                <label for="message">Message</label>
              </div>
              <input type="submit" value="Submit" class="btn">
            </div>
          </div>
        </div>

    </section>

      <!--JavaScript at end of body for optimized loading-->
    <script type="text/javascript" src="js/materialize.min.js"></script>
    
    <script type="text/javascript">
      const sideNav = document.querySelector('.sidenav');
      M.Sidenav.init(sideNav, {});
    </script>

    <script type="text/javascript">
      const slider = document.querySelector('.slider');
        M.Slider.init(slider, {
        indicators: false,
        height: 500,
        transition: 500,
        interval: 4000
      });
    </script>
    
    <script type="text/javascript">
      const ss = document.querySelectorAll('.scrollspy');
      M.ScrollSpy.init(ss, {});
    </script>
    <script type="text/javascript">
    	 document.addEventListener('DOMContentLoaded', function() {
		    var elems = document.querySelectorAll('.dropdown-trigger');
		    var instances = M.Dropdown.init(elems);
		  });
    </script>


    </body>
  </html>
        