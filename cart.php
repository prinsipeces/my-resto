<?php
session_start();
  require('./views/layouts/master.view.php');
  require('./core/functions.php');


  //check if the form is submitted
    if(!isset($_SESSION['cart'])) {
      $_SESSION['cart'] = [];
    }
    $total = 0;
?>

<!DOCTYPE html>
<head>
	<title></title>
   <script type="text/javascript" src="./js/menujs.js"></script>
   <style>
#customers {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #4CAF50;
  color: white;
}
</style>
</head>
<body>

<div class="container z-depth-5">
  <table id="customers">
  <tr>
    <th>
      <section class="container content-section">
        CART
    </th>
    <th>Name of item</th>
    <th>Quantity</th>
    <th>Price</th>

  </tr>
  <tr>
            <?php foreach ($_SESSION['cart'] as $cartitem): ?>
              <?php $total += $cartitem->price*$cartitem->quantity;?>
              <div class="cart-row">
                <td><img src="<?php echo $cartitem->img ?>" alt=""></td>
                  <td><span class="cart-item cart-header cart-column flex-child long-and-truncated"><?php echo $cartitem->name ?></span></td>
                  <td><span class="cart-price cart-header cart-column"><?php echo $cartitem->quantity ?></span></td>
                  <td><span class="cart-price cart-header cart-column"><?php echo $cartitem->price ?></span></td>

                  <tr></tr>
              </div>  
            <?php endforeach;?>
    </tr>

  </table>
            <div class="cart-items">
            </div>
            <div class="cart-total right">
                <strong class="cart-total-title">Total:</strong>
                <span class="cart-total-price">₱<?=$total?></span>
            </div></br>
            
            <a href="menu.php"><button class="btn btn-primary btn-purchase" type="button">GO BACK TO MENU</button></a>

            <a href="checkout.php"><button class=" right btn btn-primary btn-purchase" type="button">PURCHASE</button></a>
            
        </section>
</div>

</body>
</html>