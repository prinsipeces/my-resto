<?php
    require('./core/functions.php');
    //check if the form is submitted
    $errors = array();

    if(isset($_POST['submit'])) {
        //connect to the database
        $pdo = connectToDB();
        //retrive the user
        $statement = $pdo->prepare('SELECT * FROM login WHERE username = :username');
        $statement->execute([
            'username' => $_POST['username']
        ]);
        $user = $statement->fetch(PDO::FETCH_OBJ);
        //retrive the password
        $statement = $pdo->prepare('SELECT * FROM login WHERE password = :password');
        $statement->execute([
            'password' => $_POST['password']
        ]);
        $user = $statement->fetch(PDO::FETCH_OBJ);
        //check if the user exists
        if($user != null) {
            if($user->password) {
                //check if the password is correct
                session_start();
                $user->password = '';
                $_SESSION['user'] = $user;
                header('location: ./index.php');
            }  
        }
        //if not then display a basic error message 
        echo "<script>alert('Wrong username/password. Please try again.')</script>";
    }
?>
<!DOCTYPE html>
<html lang="en">

    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
        <meta name="generator" content="Jekyll v3.8.5">
        <title>Signin</title>

        <link href="./css/bootstrap.min.css" rel="stylesheet" crossorigin=" anonymous">
        <style>
            html, body {
                height: 100%;
            }
            .width-30{
                width: 30%;
            }
            .login-container {
                display: flex;
                justify-content: center;
                align-items: center;
            }
        </style>
    </head>
    <body class="text-center login-container">
        <div class="width-30">
            <form class="form-signin" method="POST">
                <h1 class="h3 mb-3 font-weight-normal">Login</h1>
                <label for="inputEmail" class="sr-only">Username</label>
                <input type="username" name='username' class="form-control" placeholder="Username" required autofocus>
                <label for="inputPassword" class="sr-only">Password</label>
                <input type="password" name='password' class="form-control" placeholder="Password" required>
                <input name ='submit' class="btn btn-lg btn-primary btn-block" type="submit"></input>
                <p class="mt-5 mb-3 text-muted">&copy; 2019-2020</p>
            </form>            
        </div>
    </body>
</html>
