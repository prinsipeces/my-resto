<?php
	session_start();
	require('./core/functions.php');
	//check if the form is submitted

		//connect to the database
		$pdo = connectToDB();
		//retrive the user
		$statement = $pdo->prepare('SELECT * FROM menuitem where id = :id');
		$statement->execute([
			'id' => $_GET['id']
		]);

		$menuitems = $statement->fetch(PDO::FETCH_OBJ);

		if(!isset($_SESSION['cart'])) {
			$_SESSION['cart'] = [];
		}
		$hasexistingitem = false;
		foreach ($_SESSION['cart'] as $cartitem){
			if ($cartitem->id == $_GET['id']) {
				$cartitem->quantity++;
				$hasexistingitem=true;
				break;
			}
		}

		if (!$hasexistingitem) {
			$menuitems->quantity=1;
			$_SESSION['cart'][]= $menuitems;
		}
		header('Location: ./menu.php');
		exit();

?>