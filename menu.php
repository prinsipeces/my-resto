<?php
session_start();
  require('./views/layouts/master.view.php');
  require('./core/functions.php');
  //check if the form is submitted
  
    $pdo = connectToDB();
    //retrive the user
    $statement = $pdo->prepare('SELECT * FROM menuitem where category = :category');
    $statement->execute([
      'category' => 'Breakfast'
    ]);
    $breakfast_items = $statement->fetchAll(PDO::FETCH_OBJ);

    $statement->execute([
      'category' => 'Rice Meals'
    ]);
    $ricemeal_items = $statement->fetchAll(PDO::FETCH_OBJ);

    $statement->execute([
      'category' => 'Chicken Meals'
    ]);
    $chickenmeal_items = $statement->fetchAll(PDO::FETCH_OBJ);

    $statement->execute([
      'category' => 'Noodles'
    ]);
    $noodles_items = $statement->fetchAll(PDO::FETCH_OBJ);

    $statement->execute([
      'category' => 'Burger'
    ]);
    $burger_items = $statement->fetchAll(PDO::FETCH_OBJ);
?>
  <!-- Breadcrumbs --> 
  <div class="col s12 container center z-depth-5">
    <a href="#!" class="breadcrumb black-text"><h2>Menu</h2></a>
    
         <div class = "col s12 z-depth-5">
            <ul class = "tabs">
               <li class = "tab col s3"><a class = "active" href = "#breakfast">Breakfast</a></li>
               <li class = "tab col s3"><a href = "#ricemeals">Rice Meals</a></li>
               <li class = "tab col s3"><a href = "#chicken">Chicken Meals</a></li>
               <li class = "tab col s3"><a href = "#noodles">Noodles</a></li>
               <li class = "tab col s3"><a href = "#burger">Burger</a></li>
            </ul>
         </div>
      
      <!-- Breakfast -->   
      <div id="breakfast">
      <div class="row">
        <?php foreach ($breakfast_items as $breakfast_item): ?>
          
        
          <div class="card">
            <div class="col s12 m3">
              <div class="card-image">
                <img src="<?php echo $breakfast_item->img ?>" alt="">
                <div><h6><span><?php echo $breakfast_item->name ?></span></h6></div>
                  <div><span class="red-text shop-item-price"><?php echo $breakfast_item->price ?></span>
                    <a href="add_to_cart.php?id=<?php echo $breakfast_item->id ?>" class="btn btn-primary" type="button">ADD TO CART</a>
                  </div>
              </div>
            </div>
          </div>
      <?php endforeach;?>

    </div>
  </div>

   <!-- Rice Meals -->
   <div id="ricemeals">
      <div class="row">
        <?php foreach ($ricemeal_items as $ricemeal_item): ?>

        <div class="card">
          <div class="col s12 m3">
            <div class="card-image">
              <img src="<?php echo $ricemeal_item->img ?>" alt="">
              <div><h6><span><?php echo $ricemeal_item->name ?></span></h6></div><br>
                <div><span class="red-text"><?php echo $ricemeal_item->price ?></span>
                <a href="add_to_cart.php?id=<?php echo $ricemeal_item->id ?>" class="btn btn-primary" type="button">ADD TO CART</a>
                </div>
            </div>
          </div>
        </div>
      <?php endforeach;?>
      
      </div>  
    </div>

<!-- Chicken Meals -->
    <div id="chicken">
      <div class="row">
        <?php foreach ($chickenmeal_items as $chickenmeal_item): ?>

        <div class="card">
          <div class="col s12 m3">
            <div class="card-image">
              <img src="<?php echo $chickenmeal_item->img ?>" alt="">
              <div><h6><span><?php echo $chickenmeal_item->name ?></span></h6></div><br>
                <div><span class="red-text"><?php echo $chickenmeal_item->price ?></span>
                <a href="add_to_cart.php?id=<?php echo $chickenmeal_item->id ?>" class="btn btn-primary" type="button">ADD TO CART</a>
                </div>
            </div>
          </div>
        </div>
      <?php endforeach;?>
        
      </div>  
    </div>

<!-- Noodles -->
    <div id="noodles">
      <div class="row">
        <?php foreach ($noodles_items as $noodles_item): ?>

        <div class="card">
          <div class="col s12 m3">
            <div class="card-image shop-item">
              <img src="<?php echo $noodles_item->img ?>" alt="">
              <div><h6><span><?php echo $noodles_item->name ?></span></h6></div>
                <div><span class="red-text"><?php echo $noodles_item->price ?></span>
                <a href="add_to_cart.php?id=<?php echo $noodles_item->id ?>" class="btn btn-primary" type="button">ADD TO CART</a>
            </div>
          </div>
        </div>
      </div>
      <?php endforeach;?>
        
      </div>  
    </div>

<!-- Burger -->
    <div id="burger">
      <div class="row">
        <?php foreach ($burger_items as $burger_item): ?>

        <div class="card">
          <div class="col s12 m3">
            <div class="card-image">
              <img src="<?php echo $burger_item->img ?>" alt="">
              <div><h6><span><?php echo $burger_item->name ?></span></h6></div>
                <div><span class="red-text"><?php echo $burger_item->price ?></span>
                <a href="add_to_cart.php?id=<?php echo $burger_item->id ?>" class="btn btn-primary" type="button">ADD TO CART</a>
              </div>
            </div>
          </div>
        </div>
        <?php endforeach;?>

      </div>
   </div> 



<meta name = "viewport" content = "width = device-width, initial-scale = 1">      
      <link rel = "stylesheet"
         href = "https://fonts.googleapis.com/icon?family=Material+Icons">
      <link rel = "stylesheet" 
         href = "https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/css/materialize.min.css">
      <script type = "text/javascript"
         src = "https://code.jquery.com/jquery-2.1.1.min.js"></script>           
      <script src = "https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/js/materialize.min.js">
      </script> 

   </body>
</html>